open System.IO

// Part 1
let fuelCost x = x / 3 - 2

// Part 2
let fuelCost2 fuel =
  let rec calculate acc x =
    let n' = fuelCost x
    if n' > 0
    then calculate (acc + n') n'
    else acc
  calculate 0 fuel

File.ReadAllLines("./2019/data/day1.txt")
|> Seq.map (int >> fuelCost2)
|> Seq.reduce (+)
