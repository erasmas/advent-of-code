open System.IO

type X = int

type Y = int

type Point = X * Y

let Left = (-1, 0)
let Right = (1, 0)
let Up = (0, 1)
let Down = (0, -1)

let steps (stepStr: string) =
    let step =
        match stepStr.[0] with
        | 'R' -> Right
        | 'L' -> Left
        | 'U' -> Up
        | 'D' -> Down
        | _ -> failwithf "Unsupported direction: %s" stepStr

    let numberOfSteps = int (stepStr.Substring(1))
    seq {
        for i in 1 .. numberOfSteps do
            yield step
    }

let walkPath (start: Point) (path: seq<Point>): seq<Point> =
    let move (x, y) (x', y') = (x + x', y + y')

    let rec loop acc last rest =
        match rest with
        | [] ->
            acc
            |> List.rev
            |> List.toSeq
        | h :: t ->
            let next = move last h
            loop (next :: acc) next t

    loop [] start (List.ofSeq path)

let parsePathInput (input: string): seq<Point> =
    input.Split [| ',' |]
    |> Seq.map steps
    |> Seq.concat

let input =
    File.ReadAllText("./2019/data/day3.txt")
    |> (fun (line: string) -> line.Split [| '\n' |])
    |> Array.filter (fun s -> String.length s > 0)
    |> Array.map parsePathInput
    |> (fun arr -> (arr.[0], arr.[1]))

let manhattanDistance ((x1, y1): Point) ((x2, y2): Point): int =
    abs (x1 - x2) + abs (y1 - y2)

let start = (0, 0)

let solve1 (steps1, steps2): int =
    let line1 = walkPath start steps1 |> set
    let line2 = walkPath start steps2 |> set
    Set.intersect line1 line2
    |> Seq.minBy (manhattanDistance start)
    |> manhattanDistance start

let solve2 (steps1, steps2): int =
    let withDistance points =
        Seq.zip points (seq { 1 .. Seq.length points }) |> Map.ofSeq

    let stepsToIntersection (distances1: Map<Point, int>) (distances2: Map<Point, int>) point =
        distances1.Item point + distances2.Item point

    let line1 = walkPath start steps1
    let line2 = walkPath start steps2
    let distances1 = withDistance line1
    let distances2 = withDistance line2
    Set.intersect (set line1) (set line2)
    |> Seq.minBy (stepsToIntersection distances1 distances2)
    |> (stepsToIntersection distances1 distances2)